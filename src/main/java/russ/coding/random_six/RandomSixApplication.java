package russ.coding.random_six;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomSixApplication implements CommandLineRunner{
	private static final Logger logger = LoggerFactory.getLogger(RandomSixApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(RandomSixApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Set<Long> randomSet = random6(42);
		logger.info("randomSet: {}", randomSet);
	}

	private static Set<Long> random6(int maxVal) {
		Set<Long> rtnSet = new HashSet<>();
		Long next = (long) (maxVal);

		while (rtnSet.size() < 6) {
			Long timestamp = System.currentTimeMillis();
			next = (timestamp * next + maxVal -1) % (maxVal + 1);
			if (next <= 0) {
				continue;
			}
			if (next > maxVal) {
				logger.error("wrong value: {}", next);
			}
			rtnSet.add(next);
		}
		return rtnSet;
	}
}
